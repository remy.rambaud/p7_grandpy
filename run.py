#! /usr/bin/env python

from grandpy import web

if __name__ == "__main__":
    web.app.run(host='0.0.0.0', debug=True)
