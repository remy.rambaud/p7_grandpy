$(function () {
	$('#userAsk').on("submit", function (event) {
		event.preventDefault();
		let $chatZone = $("#chatZone");
		$('#loadButton').show();
		$("input:submit").hide();
		$.ajax({
			url: '/data',
			data: $('#query').serialize(),
			type: 'POST',
			success: function (response) {
				console.log(response);

				let pUser = "<p class='dial-user text-right'>" + "Vous :<br>";
				let pGrandpy = "<p class='dial-grandpy text-left'>" + "Grandpy :<br>";
				let Grandpy
				let img

				if ('img' in response) {
					img = "<img class='img-fluid img-m' src='" + "static/maps/" + response["img"] + "'>";
					Grandpy = pGrandpy + response["grandpy"] +	img + "<br>" + 
						"<p class='text-justify'>" + response["extract"] + "<\p>" + 
						"<br>" + "</p>";
				} else {
					Grandpy = pGrandpy + response["grandpy"] + "<br>" + "</p>";
				}

				$chatZone.append(pUser + response["query"] + "</p>");
				$chatZone.append(Grandpy);

				// Hide loading button and show submit
				$('#loadButton').hide();
				$("input:submit").show();
				$('input:text').val('');

				setTimeout(function () {
					let $chatZone = $("#chatZone");
					let height = $chatZone[0].scrollHeight;
					height = height;
					console.log(height);
					$chatZone.scrollTop(height);
				}, 1000);

			},
			error: function (error) {
				console.log(error);
			}
		});
	});
});
