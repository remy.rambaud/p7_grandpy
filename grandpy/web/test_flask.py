from time import sleep

from flask_testing import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from . import app


class UserTest(LiveServerTestCase):
    def create_app(self):
        app.config.from_object('config')
        return app

    def setUp(self):
        """Setup the test driver"""
        self.driver = webdriver.Chrome()

    def tearDown(self):
        self.driver.quit()

    def test_query(self):
        """ Send Paris in query form, search string from wikipedia extract. """
        self.driver.get("http://localhost:5000")
        assert "GrandPy" in self.driver.title
        elem = self.driver.find_element_by_name("query")
        elem.clear()
        elem.send_keys("Paris")
        elem.send_keys(Keys.RETURN)
        sleep(3)
        assert "capitale" in self.driver.page_source
