from flask import Flask, jsonify, render_template, request

import grandpy.askme as askme

app = Flask(__name__)

app.config.from_object('config')


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/talk/')
def talk():
    return render_template('talk.html')


@app.route('/data', methods=['POST'])
def data():
    query = request.form['query']
    print('reçu :', query)

    # Initialize GrandPy response
    response = askme.Sentences()

    # Parse user's query.
    parsed = askme.Parser(query)
    print('parsed :', parsed.query)

    # Test length of query.
    if len(parsed.query) < 3:
        return jsonify({'grandpy': 'Trop court !',
                        'query': parsed.query,
                        'parsed_query': query})

    # Get map image.
    url = askme.GetMap(parsed.query)
    print(url.url)

    # Get info extract.
    info = askme.GetInfo(parsed.query)
    try:
        extract = info.extract[2][0]
        print(extract)
    except:
        extract = ['']

    if url.file_name == 0 or len(extract) <= 1:
        print(len(extract))
        return jsonify({'grandpy': response.notfind,
                        'query': parsed.query_xss,
                        'parsed_query': parsed.query})

    return jsonify({'grandpy': response.reply,
                    'query': parsed.query_xss,
                    'parsed_query': parsed.query,
                    'img': url.file_name,
                    'extract': extract})
