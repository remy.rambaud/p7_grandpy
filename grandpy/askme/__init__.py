""" The brain of Grand Py.
We are parsing query coming from user then sending it trought
Google and Mediawiki API  """

from .getinfo import GetInfo
from .getmap import GetMap
from .parser import Parser
from .config import Sets
from .config import Sentences
