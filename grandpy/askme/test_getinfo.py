import requests

from .getinfo import GetInfo


def test_get_extract(monkeypatch):
    results = [
        'Paris',
        ['Paris'],
        [
            "Paris [pa.ʁi]  est la capitale de la France. Elle se situe au\
             cœur d'un vaste bassin sédimentaire aux sols fertiles et au\
             climat tempéré, le bassin parisien, sur une boucle de la Seine,\
             entre les confluents de celle-ci avec la Marne et l'Oise."
        ],
        ['https://fr.wikipedia.org/wiki/Paris']
    ]

    def mockreturn(request, params):
        return results

    monkeypatch.setattr(requests, 'get', mockreturn)

    assert GetInfo.get_extract('Paris') == results
