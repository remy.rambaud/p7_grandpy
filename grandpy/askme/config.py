#! /usr/bin/env python3
# coding: utf-8

import json
import random


class Sets():
    """ Collection of data used as filters. """

    def __init__(self):
        self.stop_words = set(self.json_load("grandpy/askme/fr.json"))
        self.punctuation = {"?", ",", ";", ".", "(", ")",
                            ":", "/", "!", "-", "'", "<", ">"}
        self.special = {"(", ")", "/", "'\'", "<", ">"}

    @staticmethod
    def json_load(file):
        """ Return a list from stop words in json file """
        with open(file, "r", encoding="utf8") as jsonfile:
            return json.load(jsonfile)


class Maps():
    """ Config data for Google Maps API. """
    APIKEY = "&key=AIzaSyBqi8_hFUhFFgRN_kDo8br6uZIWTyoCdH4"
    BASEURL = "https://maps.googleapis.com/maps/api/staticmap?center="
    PARAMETERS = "&zoom=8&size=600x400&maptype=roadmap&language=fr&markers="


class Sentences():
    """ Random sentences from GrandPy. """
    def __init__(self):
        self.sentences = self.json_load("grandpy/askme/sentences.json")
        self.reply = random.choice(self.sentences["reply"])
        self.notfind = random.choice(self.sentences["notfind"])

    @staticmethod
    def json_load(file):
        """ Return a list from stop words in json file """
        with open(file, "r", encoding="utf8") as jsonfile:
            return json.load(jsonfile)
