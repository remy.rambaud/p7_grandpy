#! /usr/bin/env python3
# coding: utf-8

from urllib import parse
from .config import Sets


class Parser():
    """ Parse and filter query. """

    def __init__(self, query):
        # Get sets from config class.
        self.sets = Sets()
        self.stop_word = self.sets.stop_words
        self.punctuation = self.sets.punctuation

        # Keep original query for control purpose.
        self.query_origin = query
        self.query_lower = self.lower_string(query)
        self.query_filtered = self.filter_stop_words(
            self.filter_punctuation(self.query_lower)
        )

        # Final query, ready for API or webpage.
        self.query = parse.quote(" ".join(self.query_filtered))
        self.query_xss = self.filter_xss(query)

    @staticmethod
    def lower_string(query):
        """ Cut the heads off and split the body. """
        return list(query.lower().split(" "))

    def filter_punctuation(self, query):
        """ Rip off punctuation """
        all_words = []
        for string in query:
            word = []
            for character in string:
                if character not in self.punctuation:
                    word.append(" ".join(character))
                else:
                    word.append(" ")
            all_words.append("".join(word))

        # Eventualy, merge nested list.
        query_output = []
        for element in all_words:
            if " " in element:
                query_output.extend(element.split(" "))
            else:
                query_output.append(element)

        return list(filter(None, query_output))

    def filter_xss(self, query):
        """ Return a string free of special characters. """
        for character in self.sets.special:
            query = query.replace(character, ' ')

        return query.strip()

    def filter_stop_words(self, query):
        """ Keep away meaningless words. """
        return list(set(query)-self.stop_word)
