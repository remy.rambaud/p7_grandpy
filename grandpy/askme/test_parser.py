from .parser import Parser

query = "<Test! ou Test2"
parser = Parser(query)


def test_lower_string():
    assert parser.query_lower == ["<test!", "ou", "test2"]


def test_filter_punctuation():
    assert sorted(parser.query_filtered) == sorted(["test", "test2"])


def test_filter_xss():
    assert parser.query_xss == "Test! ou Test2"
