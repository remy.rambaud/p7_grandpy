#! /usr/bin/env python3
# coding: utf-8

import json

import requests


class GetInfo():
    """ Get Wikipedia infos with user's query. """

    def __init__(self, query):
        self.query = query
        self.extract = self.get_extract(query).json()

    @staticmethod
    def get_extract(query):
        """ Get article extract from Wikipedia. """

        response = requests.get(
            'https://fr.wikipedia.org/w/api.php',
            params={
                'action': 'opensearch',
                'format': 'json',
                'search': query,
                'limit': 1,
            }
        )

        return response
