from .getmap import GetMap


def test_get_img(monkeypatch):
    results = "test.png"

    def mockreturn(query):
        return results

    monkeypatch.setattr(GetMap, 'get_img', mockreturn)

    test_map = GetMap('Paris')

    assert test_map.file_name == "test.png"
