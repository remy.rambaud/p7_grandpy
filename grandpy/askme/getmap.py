#! /usr/bin/env python3
# coding: utf-8

import random

import requests

from .config import Maps


class GetMap():
    """ Get maps from Google API with user's query. """

    def __init__(self, query):
        self.url = self.create_url(query)
        self.file_name = self.get_img()

    @staticmethod
    def create_url(query):
        """ Return url with query and parameters. """
        return Maps.BASEURL + query + Maps.PARAMETERS + query + Maps.APIKEY

    def get_img(self):
        """ Download static map. """
        img = requests.get(self.url, allow_redirects=True)
        file_name = str(random.randrange(1, 10000)) + ".png"
        path_name = "grandpy/web/static/maps/"

        # Return 0 if we don't have result.
        if "X-Staticmap-API-Warning" in img.headers\
                or int(img.headers['Content-Length']) <= 3030:
            return 0

        with open(path_name + file_name, "wb") as maps:
            maps.write(img.content)

        return file_name
